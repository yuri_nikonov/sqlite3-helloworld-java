import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MessagesFill
{
    public static void main(String[] args) throws Exception
    {
        
        long durationTime = System.currentTimeMillis();
        File f = new File("messages");

        try
        {
            
            Connection connection = null;
            
            BufferedReader b = new BufferedReader(new FileReader(f));
            boolean startedTransactionFlag = false;
            
            String readLine = "";
            System.out.println("Reading file, using Buffered Reader");

            
            try
            {
            
                // Create database connection
                connection = DriverManager.getConnection("jdbc:sqlite:messages.db");
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);
                int linecount = 0;
            
                statement.executeUpdate("PRAGMA synchronous = OFF");
                statement.executeUpdate("PRAGMA journal_mode = MEMORY");
                statement.executeUpdate("drop table if exists messages");
                statement.executeUpdate("create table messages (id integer primary key autoincrement not null, datetime text, host text, source text, message text)");

                PreparedStatement pstmt = connection.prepareStatement("insert into messages (datetime, host, source, message) values (?,?,?,?)");
                
                while ((readLine = b.readLine()) != null)
                {
                    if (!startedTransactionFlag)
                    {
                        startedTransactionFlag = true;
                        statement.executeUpdate("begin transaction");
                    }
                    // Jan 10 10:00:36 workctl1 systemd: Starting Docker Cleanup...
                    String[] part = readLine.split("\\s+", 6);
                    // System.out.println(part[0] + ";" + part[1] + ";" + part[2] + ";" + part[5]);
                    pstmt.setString(1, part[0] + " " + part[1] + " " + part[2]);
                    pstmt.setString(2, part[3]);
                    pstmt.setString(3, part[4].replaceAll("\\[.*\\]:$|:$", ""));
                    pstmt.setString(4, part[5]);
                    pstmt.executeUpdate();
                    // statement.executeUpdate("insert into messages (datetime, host, source, message) values ('" + part[0] + " " + part[1] + " " + part[2] + "', '" + part[3] + "', '" + part[4] + "', '" + part[5].replaceAll("'", "''") + "')");
                    linecount++;
                    if ( linecount % 50000 == 0 )
                    {
                        startedTransactionFlag = false;
                        statement.executeUpdate("end transaction");
                        System.out.println("Processed: " + linecount + " records");
                    }
                }
                if ( startedTransactionFlag )
                {
                    statement.executeUpdate("end transaction");
                    System.out.println("Processed: " + linecount + " records");
                }
                
                float durationTimeSec = (float) ( System.currentTimeMillis() - durationTime ) / 1000;
                System.out.println("Duration time: " + durationTimeSec + " Sec., " + linecount + " records, " + (float) linecount / durationTimeSec + " records per sec.");
            }
            catch(IOException e)
            {
                System.err.println(e.getMessage());
            }
            finally
            {
                try
                {
                    if (connection != null)
                        connection.close();
                }
                catch(SQLException e)
                {
                    System.err.println(e.getMessage());
                }
            }
        }
        catch(Exception e)
        {
            System.err.println(e.getMessage());
        }
    }
}