#!/usr/bin/env bash

jdbc_url="https://repo1.maven.org/maven2/org/xerial/sqlite-jdbc/3.23.1/sqlite-jdbc-3.23.1.jar"
jdbc_file="sqlite-jdbc-3.23.1.jar"
download_command="wget $jdbc_url"

if [ ! -f $jdbc_file ]; then
    $download_command
fi

javac MessagesFill.java
